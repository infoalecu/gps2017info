﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Numerics;
using System.Globalization;

namespace CSharp_Tutorial_1
{
    class Program
    {
        static void Main(string[] args)
        {
            StringBuilder sb = new StringBuilder("Random Test");

            StringBuilder sb2 = new StringBuilder("More Stuff that is important",256);
            Console.WriteLine("Capacity : {0}", sb.Capacity);
            Console.WriteLine("Capacity : {0}", sb2.Capacity);
            Console.WriteLine("Length : {0}", sb.Length);
            sb2.AppendLine("\nMore important text");
            CultureInfo enUS = CultureInfo.CreateSpecificCulture("en-US");
            string bestCust = "Bob Smith";
            sb2.AppendFormat(enUS, "Best Customer: {0}", bestCust);
            Console.WriteLine(sb2.ToString());

            Console.ReadLine();
        }
    }
}